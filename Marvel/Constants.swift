//
//  Constants.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 30/09/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import Foundation

struct Constants {

    struct Url {
        
        static let GetUrl = "https://gateway.marvel.com/v1/public/characters"
        
    }
    
    struct Key {
        
        static let publicKey = "67b947c15e8825d365e0e9a208c0b32b"
        static let privateKey = "38cc93ae0e1d6878021c02baa574515815cfd726"
        
    }
    
    struct Color {
        static let colorSystem = "#e91d2b"
        static let colorBorderCell = "#e9e9e9"
    }

}
