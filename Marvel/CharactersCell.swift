//
//  CharactersCell.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 01/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit

class CharactersCell: UITableViewCell {

    var margin: CGFloat = 12.0

    var nameHero: UILabel = UILabel()
    var imageHero         = UIImageView()
    
    var favoriteBand: UILabel = UILabel()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.favoriteBand = UILabel(frame: CGRect(x: 140, y: self.margin, width: self.contentView.frame.size.width, height: 20))
        self.favoriteBand.textColor = UIColor(hexString: "#ffffff")
        self.favoriteBand.font = UIFont(name: "Marvel-Regular", size: 14.0)
        self.favoriteBand.numberOfLines = 0
        self.favoriteBand.isHidden = true
        self.favoriteBand.text = "Favorite"
        self.favoriteBand.backgroundColor = UIColor(hexString: Constants.Color.colorSystem)
        self.contentView.addSubview(self.favoriteBand)
        
        
        self.nameHero = UILabel(frame: CGRect(x: 140, y: self.margin * 2, width: 200, height: 60))
        self.nameHero.textColor = UIColor(hexString: "#666666")
        self.nameHero.font = UIFont(name: "Marvel-Regular", size: 18.0)
        self.nameHero.numberOfLines = 0
        self.contentView.addSubview(self.nameHero)
        
        
    }
    
    override func layoutSubviews(){
        
        self.imageView?.frame = CGRect(x:10.0,y:7.0,width:90.0,height:90.0)
    }
    

}
