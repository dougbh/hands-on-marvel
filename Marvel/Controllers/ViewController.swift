//
//  ViewController.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 30/09/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    var margin: CGFloat = 10.0
    let UItableCharacters = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    var listCharacters : [Characters]!
    var listCharactersAux : [Characters]!
    var listCharactersFavorite : [Characters]!

    var loading = LoadingView()
    var dataHero = DataHero()

    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache:NSCache<AnyObject, AnyObject>!
    private let useAutosizingCells = true
    var shouldShowSearchResults = false
    let searchController = UISearchController(searchResultsController: nil)
    
    var time = NSDate().timeIntervalSince1970.description
    var urlCaracters: String = String()
    
    let messageResult = UILabel()

    var offset: Int = 20

   var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cache = NSCache()
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor(hexString: Constants.Color.colorSystem)
        self.refreshControl.tintColor = UIColor.white
        self.refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh), for: UIControlEvents.valueChanged)
        
        navigationItem.titleView = Utl.retornaImagem()
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let orderBy = UIBarButtonItem(image:UIImage(named:"sort-reverse-alphabetical-order"), style:.plain, target:self, action:#selector(ViewController.OrderBy))
        navigationItem.rightBarButtonItem = orderBy
        
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar"
        self.searchController.searchBar.delegate = self
        definesPresentationContext = true
        
        self.UItableCharacters.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.UItableCharacters.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.UItableCharacters.register(CharactersCell.self, forCellReuseIdentifier: "Cell")
        self.UItableCharacters.isHidden = true
        self.UItableCharacters.separatorColor = UIColor.red
        self.UItableCharacters.tableHeaderView = searchController.searchBar
       
        
        if useAutosizingCells && self.UItableCharacters.responds(to: #selector(getter: UIView.layoutMargins)) {
            self.UItableCharacters.estimatedRowHeight = 60
            self.UItableCharacters.rowHeight = UITableViewAutomaticDimension
        }
        self.UItableCharacters.infiniteScrollIndicatorMargin = 0
        self.UItableCharacters.infiniteScrollTriggerOffset = 500
        self.UItableCharacters.addInfiniteScroll { [weak self] (tableView) -> Void in
                
                self?.fetchData()
            
                let indexPaths = [NSIndexPath]()
                tableView.beginUpdates()
                tableView.insertRows(at: indexPaths as [IndexPath], with: .automatic)
                tableView.endUpdates()
            
            let delayInSeconds = 1.0;
            DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
                
                tableView.finishInfiniteScroll()
            }
            
        }
    
        
        self.UItableCharacters.addSubview(refreshControl)
        
        self.view.addSubview(self.UItableCharacters)
        
        
        
        self.messageResult.frame = CGRect(x: 10, y: self.view.frame.size.height / 2, width: self.view.frame.size.width, height: 40)
        self.messageResult.text = "No records found!"
        self.messageResult.font = UIFont(name: "Marvel-Regular", size: 15.0)
        self.messageResult.textColor = UIColor(hexString: "#666666")
        self.messageResult.isHidden = true
        self.messageResult.textAlignment = .center
        self.view.addSubview(self.messageResult)
        
        loading.showAtCenterPointWithSize(CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2), size: 10.0)
        self.view.addSubview(loading)
        loading.startLoading()

       callCharacters()
      
    }
    
    func handleRefresh() {
        
        let delayInSeconds = 1.0;
        DispatchQueue.main.asyncAfter(deadline: .now() + delayInSeconds) {
            
            self.callCharacters()
            
            self.refreshControl!.endRefreshing()
        }
        
       
    }
    
    func callCharacters()
    {
        self.urlCaracters = Constants.Url.GetUrl + "?orderBy=modified&ts=\(self.time)&apikey=\(Constants.Key.publicKey)&hash=\(Utl.retornHash(self.time))"
        
        let api = CharactersAPI()
        api.GetCharacters(self.urlCaracters, completion: didLoadCharacters)
    
    }
    
    
    fileprivate func fetchData() {
        
        //self.offset
        
        self.urlCaracters = Constants.Url.GetUrl + "?orderBy=modified&offset=\(self.offset)&ts=\(self.time)&apikey=\(Constants.Key.publicKey)&hash=\(Utl.retornHash(self.time))"
        
        self.offset +=  20
        
        print(self.offset)
        
        let api = CharactersAPI()
        api.GetCharacters(self.urlCaracters, completion: didLoadCharactersFetch)

        
    }
    
    
     func didLoadCharactersFetch(_ list: [Characters]){
        
    
        self.listCharacters.append(contentsOf: list)
        
        self.UItableCharacters.reloadData()
        
        
    }
    
     func didLoadCharacters(_ list: [Characters]){
        
        self.listCharactersFavorite = dataHero.getTranscriptions()
        
        if(self.listCharactersFavorite.count > 0){
            
            self.listCharacters = self.listCharactersFavorite
            self.listCharacters.append(contentsOf: list)
            
        }else{
        
            self.listCharacters = list
        
        }
        
        loading.isHidden = true
        
        if(self.listCharacters.count > 0){
            
            self.listCharactersAux = list
            
            self.UItableCharacters.dataSource = self
            self.UItableCharacters.delegate = self
            self.UItableCharacters.isHidden = false
            self.UItableCharacters.reloadData()
            
        }else{
            
            self.listCharacters = self.listCharactersAux
            
            self.UItableCharacters.reloadData()
            
            let controller = UIAlertController(title: "Error!", message: "No records found", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Dismiss", style: .destructive) { (action) in
                
                self.loading.isHidden = true
                self.UItableCharacters.isHidden = false
                
            }
            controller.addAction(alertAction)
            present(controller, animated: true, completion: nil)
        
            
        }
        
    }
    
    func OrderBy()
    {
        self.urlCaracters = Constants.Url.GetUrl + "?orderBy=name&ts=\(self.time)&apikey=\(Constants.Key.publicKey)&hash=\(Utl.retornHash(self.time))"
        
        loading.isHidden = false
        self.UItableCharacters.isHidden = true
        
        let api = CharactersAPI()
        api.GetCharacters(self.urlCaracters.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!, completion: didLoadCharacters)
        
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return   self.listCharacters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let Identifier    = "Characters";
        var  cell:CharactersCell? = tableView.dequeueReusableCell(withIdentifier: Identifier) as? CharactersCell
        
        if (cell == nil)
        {
            let nib:Array = Bundle.main.loadNibNamed("Characters", owner: self, options: nil)!
            cell = nib[0] as? CharactersCell
        }
    
        
        cell?.nameHero.text = self.listCharacters[(indexPath as NSIndexPath).row].name
        
        cell?.imageView?.layer.cornerRadius = 45
        cell?.imageView?.layer.masksToBounds = true
        
        if(self.listCharacters[(indexPath as NSIndexPath).row].favorites == true){
        
            let myImage = self.listCharacters[(indexPath as NSIndexPath).row].imageData
            let image : UIImage = UIImage(data: myImage!)!
            
            cell?.imageView?.image = image
        
            cell?.favoriteBand.isHidden = false
            
        }else{
        
            let urlImage = self.listCharacters[(indexPath as NSIndexPath).row].image
            
            if let url = URL(string: urlImage!) {
                
                
                cell?.imageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                
            }
        
        }
        
        
        
       return cell!;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return  130;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let details = mainStoryboard.instantiateViewController(withIdentifier: "detailsHero") as! DetailsHeroController
        details.idHero = self.listCharacters[indexPath.row].id
        navigationController!.pushViewController(details, animated: true)
        
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        self.UItableCharacters.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let keyword = searchBar.text{
        
            self.urlCaracters = Constants.Url.GetUrl + "?nameStartsWith=\(keyword)&ts=\(self.time)&apikey=\(Constants.Key.publicKey)&hash=\(Utl.retornHash(self.time))"
            
            self.dismiss(animated: true, completion: nil)
            self.searchController.isActive = false
            
            loading.isHidden = false
            self.UItableCharacters.isHidden = true
            
            let api = CharactersAPI()
            api.GetCharacters(self.urlCaracters.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!, completion: didLoadCharacters)
            
        }
        
        searchController.searchBar.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension ViewController: UISearchResultsUpdating{
    @available(iOS 8.0, *)
    public func updateSearchResults(for searchController: UISearchController) {
        
        print(searchController.searchBar.text!)
        
        
    }


    
}
