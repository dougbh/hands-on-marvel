//
//  ListEventsController.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 02/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit
import SDWebImage

class ListEventsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {

    var loading = LoadingView()
    var eventsHero : [Events]!
    
    var urlEvents : String = String()
    
    let messageResult = UILabel()
    
    var flowLayout = UICollectionViewFlowLayout()
    let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "LOADING..."
        
        loadingView()
        
       
        
        let ts = NSDate().timeIntervalSince1970.description
        let urlEvents = self.urlEvents + "?ts=\(ts)&apikey=\(Constants.Key.publicKey)&hash=\(Utl.retornHash(ts))"
        
        print(urlEvents)
        
        let eventApi = EventsAPI()
        eventApi.loadEventsHero(urlEvents, completion: didLoadEventsHero)
        
        self.messageResult.frame = CGRect(x: 10, y: self.view.frame.size.height / 2, width: self.view.frame.size.width, height: 40)
        self.messageResult.text = "No records found!"
        self.messageResult.font = UIFont(name: "Marvel-Regular", size: 15.0)
        self.messageResult.textColor = UIColor(hexString: "#666666")
        self.messageResult.isHidden = true
        self.messageResult.textAlignment = .center
        self.view.addSubview(self.messageResult)
        
    }
    
    func didLoadEventsHero(_ events: [Events]){
        
        
        self.eventsHero = events
        
        navigationItem.title = "Events"
          self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Marvel-Regular", size: 20)!]

        loading.hide()
        
        if(self.eventsHero.count > 0){
        
            self.collectionView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.collectionView.collectionViewLayout = flowLayout
            self.collectionView.register(EventsCell.self, forCellWithReuseIdentifier: "EventsCell")
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
            self.view.addSubview(self.collectionView)
            self.collectionView.reloadData()
            
            
            self.messageResult.isHidden = true
            
        }else{
            
            self.messageResult.isHidden = false
        }
        
        
    }
    
    func loadingView()
    {
        loading.showAtCenterPointWithSize(CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2), size: 10.0)
        self.view.addSubview(loading)
        loading.startLoading()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.eventsHero.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let Identifier    = "EventsCell";
        
        let cell:EventsCell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier, for: indexPath) as! EventsCell
     
        cell.backgroundColor = UIColor(hexString: Constants.Color.colorBorderCell)
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 0.6
        
        let urlImage = self.eventsHero[(indexPath as NSIndexPath).row].image
        
        if let url = URL(string: urlImage!) {
            
            cell.imageEvents?.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            
        }
        
        cell.titleEvents.text = self.eventsHero[(indexPath as NSIndexPath).row].title as String
        cell.descriptionEvents.text = self.eventsHero[(indexPath as NSIndexPath).row].description as String
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            return CGSize(width: self.view.frame.width - 20, height: 490)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 10, left: 10, bottom: 5, right: 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
