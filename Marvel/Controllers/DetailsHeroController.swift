//
//  DetailsHeroController.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 02/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData

class DetailsHeroController: UIViewController {

    var loading = LoadingView()
    var Hero : [Characters]!
    
    var dataHero = DataHero()
    
    var idHero: Int = Int()
    let imageHero: UIImageView = UIImageView()

    let selectedImage = UIImage(named: "favoritos_check")
    let unselectedImage = UIImage(named: "favoritos")
    
    let favorite = UIButton(type: UIButtonType.custom)
    var isBool: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "LOADING..."
        
          self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Marvel-Regular", size: 20)!]
        
        loadingView()
        
        let ts = NSDate().timeIntervalSince1970.description
        let urlHero = Constants.Url.GetUrl + "/\(idHero)?ts=\(ts)&apikey=\(Constants.Key.publicKey)&hash=\(Utl.retornHash(ts))"
        
        let api = HeroAPI()
        api.loadDetailHero(urlHero, completion: didLoadHero)
        
        
        self.imageHero.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300)
        self.imageHero.contentMode = UIViewContentMode.scaleToFill
        self.view.addSubview(self.imageHero)
        
    }
    
    func didLoadHero(_ hero: [Characters]){
        
        self.Hero = hero
        
        if(self.Hero.count > 0){
            
            navigationItem.title = self.Hero[0].name
            
            let urlImage = self.Hero[0].image
            
            if let url = URL(string: urlImage!) {
                self.imageHero.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            }
            
            let nameHero: UILabel = UILabel(frame: CGRect(x: 15, y: self.imageHero.frame.maxY + 20, width: self.view.frame.width - 50, height: 60))
            nameHero.text = self.Hero[0].name
            nameHero.numberOfLines = 0
            nameHero.font = UIFont(name: "Marvel-Bold", size: 25.0)
            self.view.addSubview(nameHero)
            
            
            let descriptionHero: UILabel = UILabel(frame: CGRect(x: 15, y: self.imageHero.frame.maxY + 25, width: self.view.frame.width - 30, height: 200))
            descriptionHero.text = self.Hero[0].description
            descriptionHero.font = UIFont(name: "Marvel-Regular", size: 17)
            descriptionHero.numberOfLines = 0
            self.view.addSubview(descriptionHero)
            
            let events = UIButton(type: UIButtonType.custom)
            events.frame = CGRect(x: 10, y: self.view.frame.height - 60, width: self.view.frame.width - 20, height: 47)
            events.setTitleColor(UIColor(hexString: Constants.Color.colorSystem), for: UIControlState())
            events.layer.borderWidth = 1
            events.layer.borderColor = UIColor(hexString: Constants.Color.colorSystem)?.cgColor
            events.setTitle("LIST EVENTS", for: UIControlState())
            events.contentHorizontalAlignment = .center
            events.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            events.titleLabel?.font = UIFont(name: "Marvel-Bold", size: 25.0)
            events.addTarget(self, action: #selector(self.fecthEvents), for:.touchUpInside)
            self.view.addSubview(events)
            
            
            self.favorite.frame =  CGRect(x: self.view.frame.size.width - 50, y: self.imageHero.frame.maxY + 10, width: 40, height: 40)
            self.favorite.setImage(self.unselectedImage, for: UIControlState())
            self.favorite.addTarget(self, action: #selector(self.saveFavorite), for:.touchUpInside)
            self.view.addSubview(self.favorite)
            
            let isExist  = isFavorite(id: self.Hero[0].id)
            
            if(isExist){
                self.isBool = true
                self.favorite.setImage(self.selectedImage, for: UIControlState())
                print("EXISTE")
            }else{
                self.isBool = false
                print("NAO EXISTE")
            }
            
            loading.isHidden = true
        }
        
    }
    
    //MARK: FAVORITE
    func saveFavorite(){
    
        if self.isBool == true{
            //REMOVE
            dataHero.removeFavorites(id: self.Hero[0].id)
            
            self.isBool = false
            self.favorite.setImage(unselectedImage, for: UIControlState())
            
        }else{
            //SALVA
            load_image(self.Hero[0].image)
            
            self.isBool = true
            self.favorite.setImage(selectedImage, for: UIControlState())
        }
    
    }
    
    
    func load_image(_ urlString:String)
    {
        let imgURL: URL = URL(string: urlString)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func storeHero () {
                    let context = self.dataHero.getContext()
   
                    let entity =  NSEntityDescription.entity(forEntityName: "Heros", in: context)
                    
                    let transc = NSManagedObject(entity: entity!, insertInto: context)
                    
                    transc.setValue(self.Hero[0].id, forKey: "id")
                    transc.setValue(self.Hero[0].name, forKey: "name")
                    transc.setValue(self.Hero[0].description, forKey: "descriptionHero")
                    transc.setValue(data, forKey: "image")
                   
                    do {
                        try context.save()
                        
                        print("saved!")
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    } catch {
                        
                    }
                }

                
                DispatchQueue.main.async(execute: storeHero)
            }
            
        })
        
        task.resume()
    }
    
    
    
    func isFavorite(id: Int) -> Bool{
        
        var isExists = false
        
        let idHero: Int = id
        
        let predicate = NSPredicate(format: "id == %@", "\(idHero)")
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Heros")
        fetchRequest.predicate = predicate
        
        do {
            
            let searchResults = try dataHero.getContext().fetch(fetchRequest)
            
            print ("num of results = \(searchResults.count)")
            
            if(searchResults.count > 0){
                isExists = true
            }else{
                isExists = false
            }
            
        } catch {
            print("Error with request: \(error)")
        }
        return isExists
        
    }
    
    
    func fecthEvents()
    {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let details = mainStoryboard.instantiateViewController(withIdentifier: "listEvents") as! ListEventsController
        
        details.urlEvents = self.Hero[0].urlEvents
        navigationController!.pushViewController(details, animated: true)
        
    }
    
    func loadingView()
    {
        loading.showAtCenterPointWithSize(CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2), size: 10.0)
        self.view.addSubview(loading)
        loading.startLoading()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
