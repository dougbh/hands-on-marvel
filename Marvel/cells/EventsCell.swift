//
//  EventsCell.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 02/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit

class EventsCell: UICollectionViewCell {

    var titleEvents: UILabel!
    var descriptionEvents: UILabel!
    var imageEvents: UIImageView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        self.imageEvents = UIImageView(frame: CGRect(x: 0, y: 5, width: frame.size.width, height: 350))
        self.imageEvents.contentMode = UIViewContentMode.scaleAspectFit
        self.contentView.addSubview(self.imageEvents)
        
        self.titleEvents = UILabel(frame: CGRect(x: 0, y: self.imageEvents.frame.maxY + 3, width: self.contentView.frame.size.width, height: 30))
        self.titleEvents.font = UIFont(name: "Marvel-Bold", size: 16.0)
        self.titleEvents.textAlignment = .center
        self.contentView.addSubview(self.titleEvents)
        
        self.descriptionEvents = UILabel(frame: CGRect(x: 10, y: self.titleEvents.frame.maxY - 10, width: self.contentView.frame.size.width - 20, height: 100))
        self.descriptionEvents.font = UIFont(name: "Marvel-Regular", size: 14.0)
        self.descriptionEvents.numberOfLines = 0
        self.contentView.addSubview(self.descriptionEvents)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
