//
//  Characters.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 01/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit

class Characters: UITableViewCell {

    var margin: CGFloat = 12.0
    
    var nameHero: UILabel = UILabel()
    var imageHero         = UIImageView()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
