//
//  Characters.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 30/09/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit

class Characters {
    
    var id: Int!
    var name: String!
    var description: String!
    var image: String!
    var imageData: Data!
    var urlEvents: String!
    
    var favorites: Bool!

    
    init(Id: Int, name: String, description: String, image: Data){
        
        self.id = Id
        self.name = name
        self.description = description
        self.imageData = image
        self.favorites = true
        
    }
    
     init(data: NSDictionary){
     
        self.id   = data["id"] as! Int
        self.name =  Utl.getStringFromJSON(data,  key: "name")
        self.description =  Utl.getStringFromJSON(data,  key: "description")
        self.favorites = false
        
        if let imagePath = data["thumbnail"] as? NSMutableDictionary{
        
           if let path = imagePath["path"]{
                if let extensionFile = imagePath["extension"]{
                    self.image = "\(path).\(extensionFile)"
                }
            }
        
        }
        
        if let events = data["events"] as? NSMutableDictionary{
            
            if let url = events["collectionURI"]{
            
                self.urlEvents = url as? String
            }
        }
        
        
    }
    
    
}
