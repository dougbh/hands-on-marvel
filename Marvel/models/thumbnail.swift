//
//  ImageHero.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 01/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import Foundation

class thumbnail{
        
    var path: String
    var extensionPath: String
    
    init(data : NSDictionary) {
     
        self.path  = Utl.getStringFromJSON(data, key: "path")
        self.extensionPath   = Utl.getStringFromJSON(data, key: "extension")
        
    }
    

}
