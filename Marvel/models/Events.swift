//
//  Events.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 02/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import Foundation


class Events {
    
    var id: Int!
    var title: String!
    var description: String!
    var image: String!
    
    init(data: NSDictionary){
     
        self.id   = data["id"] as! Int
        self.title =  Utl.getStringFromJSON(data,  key: "title")
        self.description =  Utl.getStringFromJSON(data,  key: "description")
        
        if let imagePath = data["thumbnail"] as? NSMutableDictionary{
            
            if let path = imagePath["path"]{
                if let extensionFile = imagePath["extension"]{
                    self.image = "\(path).\(extensionFile)"
                }
            }
            
        }
        
    }
    
    
}
