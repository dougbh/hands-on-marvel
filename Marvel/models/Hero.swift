//
//  Hero.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 03/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import Foundation

class Hero {
    
    var id: Int!
    var name: String!
    var description: String!
    var image: Data!
    var urlEvents: String!
    
    
    init(Id: Int, name: String, description: String, image: Data, urlEvents: String){
    
        self.id = Id
        self.name = name
        self.description = description
        self.image = image
        self.urlEvents = urlEvents
        
    
    }
    
    
}
