//
//  Data.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 02/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import Foundation
import CoreData

class DataHero{
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }

    func getTranscriptions () -> [Characters] {
        
        var ItensCaracters = [Characters]()
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Heros")
    
        do {
            
            let searchResults = try self.getContext().fetch(fetchRequest)
            
            print ("num of results = \(searchResults.count)")
            
            for trans in searchResults as! [NSManagedObject] {
                
                let hero = Characters(Id: trans.value(forKey: "id") as! Int, name: trans.value(forKey: "name") as! String, description: trans.value(forKey: "descriptionHero") as! String, image: (trans.value(forKey: "image") as! NSData) as Data)
                
                ItensCaracters.append(hero)
                
                print("\(trans.value(forKey: "name"))")
            }
        } catch {
            print("Error with request: \(error)")
        }
        
        return ItensCaracters
    }
    
    func removeFavorites (id: Int) {
        
        let context = self.getContext()
        
        let idHero: Int = id
        
        let predicate = NSPredicate(format: "id == %@", "\(idHero)")
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Heros")
        fetchRequest.predicate = predicate
        
        do {
            let fetchedResults = try context.fetch(fetchRequest) as! [NSManagedObject]
            let entityToDelete = fetchedResults.first
            context.delete(entityToDelete!)
            
            try context.save()
            
        } catch let error as NSError {
            print("Fetch failed: \(error.localizedDescription)")
        }
    }
    
}
