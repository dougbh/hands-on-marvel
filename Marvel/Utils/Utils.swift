//
//  Utils.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 30/09/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import UIKit

class Utl{
    
    static func retornaImagem() -> UIImageView{
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "Marvel-logo")
        imageView.image = image
    
        return imageView
    
    }
    
    static func md5(_ string: String) -> String {
        
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
    
    static func getStringFromJSON(_ data: NSDictionary, key: String) -> String{
        
        if let info = data[key] as? String{
            
            return info
        }
        
        return ""
    }
    
    
    static func retornHash(_ ts: String) -> String{

        let hash = Utl.md5("\(ts)\(Constants.Key.privateKey)\(Constants.Key.publicKey)")
        
        return hash
        
    }
    
    static func scaleUIImageToSize(_ image: UIImage, size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }

    

}

