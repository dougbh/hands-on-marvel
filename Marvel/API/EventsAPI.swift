//
//  EventsAPI.swift
//  Marvel
//
//  Created by Douglas  Goulart Nunes on 02/10/16.
//  Copyright © 2016 Douglas  Goulart Nunes. All rights reserved.
//

import Foundation

class EventsAPI{
    
    func loadEventsHero(_ shotsUrl: String, completion: (([Events]) -> Void)!) {
        
        let session  = URLSession.shared
        let url = URL(string: shotsUrl)
        
        
        let task = session.dataTask(with: url!, completionHandler: {
            (data, response, error) -> Void in
            
            let httpResponse = response as! HTTPURLResponse
            let statusCode = httpResponse.statusCode
            
            
            if (statusCode == 200) {
                
                if error != nil{
                    print(error!.localizedDescription)
                }else{
                    
                    var error: NSError?
                    
                    do {
                        
                        let contentData: AnyObject = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as AnyObject
                        
                        if let dadosArray = contentData["data"] as? NSDictionary {
                            
                            if let results = dadosArray["results"] as? NSArray{
                                
                                var ItensEvents = [Events]()
                                
                                for item in results {
                                    
                                    let ev = Events(data: item as! NSDictionary)
                                    ItensEvents.append(ev)
                                }
                                
                                DispatchQueue.main.sync {
                                    completion(ItensEvents)
                                }
                                
                            }
                            
                            
                            
                        }
                        
                        
                        
                    } catch let error1 as NSError {
                        error = error1
                        print("Could not parse JSON: \(error!)")
                    } catch {
                        fatalError()
                    }
                    
                }
                
            }
            
            
            
            
        })
        task.resume()
    }

    
}
